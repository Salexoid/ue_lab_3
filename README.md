# LR2

![Scene preview](./preview_ue4.png)

![Model preview](./preview_blender.jpg)

Developed with Unreal Engine 4

Цели: приобрести навыки работы с САПР Blender для создания 3D моделей для игр.
Задачи:
- Выбрать одну из шахматных фигур.
- Создать 3D модель этой фигуры в Blender.
- Сделать развертку этой модели в Blender.
- Импортировать полученную модель в Unreal Engine 4.
- Залить проект в открытый репозиторий.

